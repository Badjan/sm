<?php

class Product {
    // повертає одну новину
    public static function getProductsByCategoryId($categoryId)
    {
        $id = intval($categoryId);
        
        if($id) {
 
            $db = Db::getConnection();
            
            $result = $db->query('SELECT * FROM products WHERE category_id=' . $id);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            
            $products= [];
            $i = 0;
            while ($row = $result->fetch()) {
            $products[$i]['size'] = $row['size'];
            $products[$i]['price'] = $row['price'];
            $i++;       
            }
            
            return $products;
        }
        
        
    }
    // повертає список новин
    public static function getProductsList()
    {
        $db = Db::getConnection();
        
        $productsList = [];
        
        $result = $db->query('SELECT product_id, solution_id, category_id, category_name, '
                . 'img_path, size, stone, price FROM products');
        
        $i = 0;
        while ($row = $result->fetch()) {
            $productsList[$i]['product_id'] = $row['product_id'];
            $productsList[$i]['solution_id'] = $row['solution_id'];
            $productsList[$i]['category_id'] = $row['category_id'];
            $productsList[$i]['category_name'] = $row['category_name'];
            $productsList[$i]['img_path'] = $row['img_path'];
            $productsList[$i]['size'] = $row['size'];
            $productsList[$i]['stone'] = $row['stone'];
            $productsList[$i]['price'] = $row['price'];
            $i++;
        }
        
        return $productsList;
    }
    
     public static function getCategories1()
    {
        $db = Db::getConnection();
        
        $categoryList = [];
        
        $result = $db->query('SELECT solution_id, category_id, category_name '
                . 'FROM categories');
        
        
        $i = 0;
        while ($row = $result->fetch()) {
            if ($row['solution_id'] == 1) {
                $categoryList[$i]['solution_id'] = $row['solution_id'];
                $categoryList[$i]['category_id'] = $row['category_id'];
                $categoryList[$i]['category_name'] = $row['category_name'];
                $i++;
            } else{
                $i++;
            }
            
        }
        
        return $categoryList;
    }
    public static function getCategories2()
    {
        $db = Db::getConnection();
        
        $categoryList = [];
        
        $result = $db->query('SELECT solution_id, category_id, category_name '
                . 'FROM categories');
        
        $i = 0;
        while ($row = $result->fetch()) {
            if ($row['solution_id'] == 2) {
                $categoryList[$i]['solution_id'] = $row['solution_id'];
                $categoryList[$i]['category_id'] = $row['category_id'];
                $categoryList[$i]['category_name'] = $row['category_name'];
                $i++;
            } else{
                $i++;
            }
        }
        
        return $categoryList;
    }
    
    public static function getProductsByCategoryAndStone($categoryId=1, $stoneId=1)
    {
        $db = Db::getConnection();
        
        $productsList = [];
        
        $result = $db->query('SELECT product_id, solution_id, category_id, category_name, '
                . 'img_path, size, stone_id, stone, price FROM products');
        
        $i = 0;
        while ($row = $result->fetch()) {
            if ($row['category_id'] == $categoryId && 
                    $row['stone_id'] == $stoneId) {
                $productsList[$i]['product_id'] = $row['product_id'];
                $productsList[$i]['solution_id'] = $row['solution_id'];
                $productsList[$i]['category_id'] = $row['category_id'];
                $productsList[$i]['category_name'] = $row['category_name'];
                $productsList[$i]['img_path'] = $row['img_path'];
                $productsList[$i]['size'] = $row['size'];
                $productsList[$i]['stone_id'] = $row['stone_id'];
                $productsList[$i]['stone'] = $row['stone'];
                $productsList[$i]['price'] = $row['price'];
                $i++;
                    } else {
                        $i++;
                    }
        }
        
        return $productsList;
        }

    }
    

