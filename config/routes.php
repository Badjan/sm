<?php
return array (
    //''                          => 'home/home/home',
    'home'                      => 'home',
    'products/([0-9]+)'         => 'products/view/$1',
    'products'                  => 'products/list',
    'contacts'                  => 'contacts/view',
);
