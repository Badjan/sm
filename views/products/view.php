<!DOCTYPE html>
<html>
<head>
  <title>StoneMart</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
  <link rel="stylesheet" href="../../styles/style.css">
  
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<?php include (ROOT . '/views/rep/nav.php')?>
    
    
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-3">
                <?php include (ROOT . '/views/rep/categories.php')?>
            </div>
            <div class="col-12 col-sm-9">
                <div class="row text-center">
                    <div class="col-12 col-sm-2"></div>
                    <div class="col-12 col-sm-2">
                        <a href="/products/<?= explode('/', $_SERVER['REQUEST_URI'])[2]?>/1"
                           ><img id="1" class="stoneType img-fluid" src="../../img/1.jpg"></a>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="/products/<?= explode('/', $_SERVER['REQUEST_URI'])[2]?>/2"
                           ><img id="2" class="stoneType img-fluid" src="../../img/2.jpg"></a>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="/products/<?= explode('/', $_SERVER['REQUEST_URI'])[2]?>/3"
                           ><img id="3" class="stoneType img-fluid" src="../../img/3.jpg"></a>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="/products/<?= explode('/', $_SERVER['REQUEST_URI'])[2]?>/4"
                           ><img id="4" class="stoneType img-fluid" src="../../img/4.jpg"></a>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-12 col-sm-2"></div>
                    <div class="col-12 col-sm-2">
                        <a href="/products/<?= explode('/', $_SERVER['REQUEST_URI'])[2]?>/5"
                           ><img id="5" class="stoneType img-fluid" src="../../img/5.jpg"></a>
                    </div>
                    <div class="col-12 col-sm-2">    
                        <a href="/products/<?= explode('/', $_SERVER['REQUEST_URI'])[2]?>/6"
                           ><img id="6" class="stoneType img-fluid" src="../../img/6.jpg"></a>
                    </div>    
                    <div class="col-12 col-sm-2">
                        <a href="/products/<?= explode('/', $_SERVER['REQUEST_URI'])[2]?>/7"
                           ><img id="7" class="stoneType img-fluid" src="../../img/7.jpg"></a>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="/products/<?= explode('/', $_SERVER['REQUEST_URI'])[2]?>/8"
                           ><img id="8" class="stoneType img-fluid" src="../../img/8.jpeg"></a>
                    </div>
                </div>      
        
                <br>
        
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="text-center">
                        <img class="rounded mx-auto d-block img-fluid" src="../../img/gfull.jpg">
                    </div>
                </div>
            </div>
                <br>
            <div class="row">
                <button class="mx-auto d-block">Замовити зараз!</button>
            </div>
                <br>
            <div class="row">
                <div class="col-12 col-sm-2"></div>
                <div class="col-12 col-sm-8">
                    <table class="table">
                        <thead class="thead-inverse">
                            <tr class="success active"><th class="text-center">РОЗМІР</th><th class="text-center">ЦІНА</th></tr>
                        </thead>
                        <tbody>
                            <?php foreach ($productsList as $product): ?>
                            <tr class="text-center"><td><?= $product['size']; ?></td><td><?= $product['price']; ?></td></tr>
                            <?php endforeach; ?>  
                        </tbody>
                    </table>
                </div>
            </div>
                <br>
            <div class="row">
                <button class="mx-auto d-block">Замовити зараз!</button>
            </div>

        </div>
    </div>
    </div> 
</div> 
                
 
<?php include (ROOT . '/views/rep/footer.php')?>
    

  
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

<script>
$(document).ready(function(){
    var url = window.location.href.substr(-1);
    $('#'+url).addClass("stoneTypeActive");
 });
</script>

</body>
</html>


