<nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="nav">
  <a class="navbar-brand" href="http://sm/home">StoneMart <small class="text-muted">
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ваші бажання втілені в камінь!</small></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="http://sm/products/list">Каталог</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="http://sm/contacts/view">Контакти</a>
      </li>
    </ul>
  </div>
</nav>

<br/>
