<div class="container">


  <div class="row">
    <div class="col-6 col-md-2">
      <div class="row text-center">
        <div class="col-6">
          <img class="img-fluid" src="../img/pay/phone.jpg" alt="Ви робите замовлення">
          <p class="text-uppercase text-center">
            Робите замовлення
          </p>
        </div>
        <div class="col-6">
          <img class="img-fluid align-middle" src="../img/pay/arrow.jpg" alt="Далі">
        </div>
      </div>
     
    </div>

    <div class="col-6 col-md-2">
      <div class="row">
        <div class="col-6">
          <img class="img-fluid" src="../img/pay/calc.jpg" alt="Дізнаєтесь вартість">
          <p class="text-uppercase text-center">
            Дізнаєтесь вартість
          </p>
        </div>
        <div class="col-6">
          <img class="img-fluid" src="../img/pay/arrow.jpg" alt="Далі">
        </div>
      </div>
    </div>

    <div class="col-6 col-md-2">
      <div class="row">
        <div class="col-6">
          <img class="img-fluid" src="../img/pay/card.jpg" alt="Сплачуєте половину вартості">
          <p class="text-uppercase text-center">
            Сплачуєте половину вартості
          </p>
        </div>
        <div class="col-6">
          <img class="img-fluid" src="../img/pay/arrow.jpg" alt="Далі">
        </div>
      </div>
    </div>

    <div class="col-6 col-md-2">
      <div class="row">
        <div class="col-6">
          <img class="img-fluid" src="../img/pay/hammer.jpg" alt="Замовлення виконується">
          <p class="text-uppercase text-center">
          Замовлення виконується
        </p>
        </div>
        <div class="col-6">
          <img class="img-fluid" src="../img/pay/arrow.jpg" alt="Далі">
        </div>
      </div>
    </div>

    <div class="col-6 col-md-2">
      <div class="row">
        <div class="col-6">
          <img class="img-fluid" src="../img/pay/card.jpg" alt="Сплачується решта вартості">
          <p class="text-uppercase text-center">
            Сплачуєте решту вартості
          </p>
        </div>
        <div class="col-6">
          <img class="img-fluid" src="../img/pay/arrow.jpg" alt="Далі">
        </div>
      </div>
    </div>

    <div class="col-6 col-md-2">
      <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
          <img class="img-fluid" src="../img/pay/pallet.jpg" alt="Ми відвантажуємо Вам готові вироби">
        </div>
        
      </div>
      <div class="row">
        <p class="text-uppercase text-center">
          Ми відвантажуємо Вам готові вироби
        </p>
      </div>
    </div>
  </div>


</div>

<!--
<div class="col-6">
          <img class="img-fluid" src="../img/pay/arrow.jpg" alt="Далі">
        </div>
-->