<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
        <img class="d-block w-100 rounded" src="/img/1c.jpg" alt="ГАБРО">
        <div class="carousel-caption d-none d-md-block">
          <h3>ГАБРО</h3>
          <p>Універсальний камінь. Від будівництва до пам'ятників</p>
        </div>
    </div>
    <div class="carousel-item">
        <img class="d-block w-100 rounded" src="/img/2c.jpg" alt="ЛЄЗНІК">
        <div class="carousel-caption d-none d-md-block">
            <h3>ЛЄЗНІК</h3>
            <p>Елітний камінь червоного кольору з дрібним зерном</p>
        </div>
    </div>
    <div class="carousel-item">
        <img class="d-block w-100 rounded" src="/img/3c.jpg" alt="ПОКОСТ">
        <div class="carousel-caption d-none d-md-block">
            <h3>ПОКОСТ</h3>
            <p>Відмінно підходить для будівельних цілей</p>
        </div>
    </div>
    <div class="carousel-item">
        <img class="d-block w-100 rounded" src="/img/4c.jpg" alt="ЛАБРАДОРИТ">
        <div class="carousel-caption d-none d-md-block">
            <h3>ЛАБРАДОРИТ</h3>
            <p>Переливчаті вкраплення ідеальні для облицювання</p>
        </div>
    </div>
    <div class="carousel-item">
        <img class="d-block w-100 rounded" src="/img/5c.jpg" alt="ТОК">
        <div class="carousel-caption d-none d-md-block">
            <h3>ТОК</h3>
            <p>Червоний граніт з щедрим чорним вкрапленням</p>
        </div>
    </div>
    <div class="carousel-item">
        <img class="d-block w-100 rounded" src="/img/6c.jpg" alt="МАСЛАВКА">
        <div class="carousel-caption d-none d-md-block">
            <h3>МАСЛАВКА</h3>
            <p>Граніт, який цінується за особливий зелений колір</p>
        </div>
    </div>
    <div class="carousel-item">
        <img class="d-block w-100 rounded" src="/img/7c.jpeg" alt="КАПУСТА">
        <div class="carousel-caption d-none d-md-block">
            <h3>КАПУСТА</h3>
            <p>Камінь з широкою чорно-червоною фактурою</p>
        </div>
    </div>
    <div class="carousel-item">
        <img class="d-block w-100 rounded" src="/img/8c.jpg" alt="МЕЖИРІЧКА">
        <div class="carousel-caption d-none d-md-block">
            <h3>МЕЖИРІЧКА</h3>
            <p>Граніт приємного персиково-коричневого кольору</p>
        </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
