<!DOCTYPE html>
<html lang="en">
<head>
  <title>StoneMart</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
  <link rel="stylesheet" href="../../styles/style.css">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">


<?php include (ROOT . '/views/rep/nav.php')?>
    <div class="container">
        <div class="row">
            <div class="col-12 col-xl-4 text-center">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2547.618658034399!2d29.0519959!3d50.3177082!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x472c8184039a57a9%3A0xd3257d357c8c9f8e!2z0LLRg9C70LjRhtGPINCS0L7Qu9C-0LTQuNC80LjRgNGB0YzQutCwLCAxMCwg0JrQvtGA0L7RgdGC0LjRiNGW0LIsINCW0LjRgtC-0LzQuNGA0YHRjNC60LAg0L7QsdC70LDRgdGC0YwsIDEyNTAx!5e0!3m2!1suk!2sua!4v1503942763538" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="col-12 col-xl-3"></div>
            <div class="col-12 col-xl-5">
                    <br>
                    <br>
                    <br>
                    <h1 class="text-center">Контакти</h1>
                    <br>
                    
                    <dl class="row">
                        <dt class="col-sm-3">Адреса:</dt>
                        <dd class="col-sm-9 text-center">Житомирська обл., м.Коростишів, вул.Володимирська 10</dd>
                        <dt class="col-sm-3">Телефон:</dt>
                        <dd class="col-sm-9 text-center">+380 67 996 80 20</dd>
                        <dt class="col-sm-3">Телефон:</dt>
                        <dd class="col-sm-9 text-center">+380 67 996 80 20</dd>
                        <dt class="col-sm-3">E-mail:</dt>
                        <dd class="col-sm-9 text-center">spp140780@gmail.com</dd>
                        <dt class="col-sm-3">E-mail:</dt>
                        <dd class="col-sm-9 text-center">spp140780@gmail.com</dd>
                        <dt class="col-sm-3">Граверстоун:</dt>
                        <dd class="col-sm-9 text-center"><a target="_blank" href="http://graverstone.ru/topic/10948-mestnyiy-kamen-opt-i-roznitsa-bez-posrednikov/?page=2&tab=comments#comment-387945">Петро Павлович</a></dd>
                    </dl>
                    
            </div>
        </div>
    </div>
    
    <br>
    
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                
                <form action="mail.php" method="post">
                    <input type="text" name="fio" placeholder="Укажите ФИО" required>
                    <input type="text" name="email" placeholder="Укажите e-mail" required>
                    <input type="submit" value="Отправить">
                </form>
                <!--<form>
                    <input type="text" name="name" placeholder="Ваше имя" required /><br />
                    <input type="text" name="phone" placeholder="Ваш телефон" required /><br />
                    <textarea rows="10" cols="45" name="text"></textarea><br />
                    <button class="mx-auto d-block">ЗВ'ЯЗАТИСЬ З НАМИ</button>
                </form>-->
            </div>
        </div>
    </div>
<?php include (ROOT . '/views/rep/footer.php')?>
    
    
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<!--<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
	
<script>
    $(document).ready(function() {

	//E-mail Ajax Send
	$("#form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			alert("Thank you!");
			setTimeout(function() {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
	});

});
</script>-->

</body>
</html>