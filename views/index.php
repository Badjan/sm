<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--Fonts-->
        <link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Archivo+Black|Libre+Baskerville|Orbitron|Yanone+Kaffeesatz" rel="stylesheet">
        <link rel="stylesheet" href="../styles/style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <style> 
            p.text-uppercase {
                font-size: 1.75vh;
            }
        </style>
    </head>
    <body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<?php include (ROOT . '/views/rep/nav.php')?>
   
    <div class="row">
        <div class="col-12 col-sm-2"></div>
        <div class="col-12 col-sm-8">
            <?php include (ROOT . '/views/rep/carousel.php')?>
        </div>
    </div>
</div>
        
        <br>
        <br>
        
        <!--НЕвеличкий текст з картинками-->
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-2"></div>
                <div class="col-12 col-sm-8">
                    <p id="home-text" class="text-justify">
                        Цей сайт створено для того, щоб полегшити Вам вибір 
                        потрібного виробу з каменю. В ньому Ви знайдете вісім 
                        видів граніту, які застосовуються в будівництві та 
                        меморіальних комплексах.
                    </p>
                </div>
            </div>
            
            <div class="row">
                <div class="col-12 col-sm-2"></div>
                <div class="col-12 col-sm-6">
                    <p id="home-text" class="text-justify">
                        Наша мета - зробити процес від вибору до доставки 
                        якомога легшим. В результаті Ви отримуєте продукцію 
                        високої якості з мінімальними зусиллями.
                    </p>
                    <p id="home-text" class="text-justify">
                        Катало продукції знаходиться у вкладці "Каталог" зверху.
                    </p>
                </div>
                <div class="col-12 col-sm-2">
                    <img class="rounded mx-auto d-block img-fluid" src="../../img/ven1.png">
                </div>
            </div>
            
            <div class="row">
                <div class="col-12 col-sm-2"></div>
                <div class="col-12 col-sm-2">
                    <img class="rounded mx-auto d-block img-fluid" src="../../img/mount.png">
                </div>
                <div class="col-12 col-sm-6">
                        <p id="home-text" class="text-justify">
                        Якщо того, що потрібно Вам в каталозі не знайшлось, 
                        зв'яжіться з нами будь-яким зручним для Вас способом і 
                        ми надамо Вам консультацію.
                    </p>
                </div>
            </div>
        </div>    
        
        <!--Розмітка для способу оплати-->
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-2"></div>
                <div class="col-12 col-sm-8">
                    <h2 class="text-center">СПОСІБ ОПЛАТИ</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-2"></div>
                <div class="col-12 col-sm-8">
                    <div class="row">
                        <div class="col-6 col-sm-1">
                            <div class="item text-center">
                                <img src=""/>
                                <p class="text-uppercase">

                                </p>
                            </div>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
        
        <br>
        
        <?php include (ROOT . '/views/rep/payment.php')?>
        
        <br>
        
        <!--Ненав'язливі контакти-->
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-2"></div>
                <div class="col-12 col-sm-8">
                    <h2 class="text-center"><a class="black" href="http://sm/contacts/view">ЗВ'ЯЗОК З НАМИ</a></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-3"></div>
                <div class="col-12 col-sm-7">
                    <br>
                    <dl class="row">
                        <dt class="col-sm-3">Телефон:</dt>
                        <dd class="col-sm-9 text-center">+380 67 996 80 20</dd>
                        <dt class="col-sm-3">Телефон:</dt>
                        <dd class="col-sm-9 text-center">+380 67 996 80 20</dd>
                        <dt class="col-sm-3">E-mail:</dt>
                        <dd class="col-sm-9 text-center">spp140780@gmail.com</dd>
                    </dl>
                </div>
            </div>
        </div>
        
<?php include (ROOT . '/views/rep/footer.php')?>
        
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    </body>
</html>