<?php

// відображення помилок
ini_set('display_errors', 1);
error_reporting(E_ALL);

// підключення файлів системи
define('ROOT', dirname(__FILE__));
require_once(ROOT.'/components/Router.php');
require_once(ROOT.'/components/Db.php');


// Виклик Router-а
$router = new Router();
$router->run();
if ($_SERVER['REQUEST_URI']==="/"){
include (ROOT . '/views/index.php');
};
