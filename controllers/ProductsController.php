<?php

include_once ROOT . '/models/Product.php';

class ProductsController
{
   
    
    public function actionList()
    {
        $productsList = [];
        $productsList = Product::getProductsList();
        
        $categoryList1 = Product::getCategories1();
        $categoryList2 = Product::getCategories2();
        
        //echo '<pre>';
        //    print_r($categoryList);
        //    echo '<pre>';
        
        require_once(ROOT . '/views/products/list.php');
        
        return TRUE;
    }
    
    public function actionView($categoryId, $stoneId)
    {
        if ($categoryId) {
            $products = Product::getProductsByCategoryId($categoryId);
            $categoryList1 = Product::getCategories1();
            $categoryList2 = Product::getCategories2();
            $productsList = Product::getProductsByCategoryAndStone($categoryId, $stoneId);
            
            require_once(ROOT . '/views/products/view.php');
        }
        $category = $categoryId;
        $stone = $stoneId;
        
        return TRUE;
    }
}