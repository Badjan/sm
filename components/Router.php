<?php


class Router 
{
    private $routes;
    
    public function __construct() 
    {
        $routesPath = ROOT.'/config/routes.php';
        $this->routes = include($routesPath);
    }
    
    // метод повертає рядок запиту
    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

        public function run() 
    {
        // отримати рядок запиту
        $uri = $this->getURI();
        
        // перевірка наявності запиту в routes.php
        foreach ($this->routes as $uriPattern => $path) {
            // порівнюємо $uriPattern i $uri
            if (preg_match("~$uriPattern~", $uri)){
                // отримуємо внутрішній шлях із зовнішнього
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);
                
                // визначає який controller та action оброблюють запит
                $segments = explode('/', $internalRoute);
                $controllerName = ucfirst(array_shift($segments).'Controller');
                $actionName = 'action'.ucfirst(array_shift($segments));
                
                $parameters = $segments;
                
                
                // підключити файл класу контролера
                $controllerFile = ROOT . '/controllers/' . 
                        $controllerName . '.php';
                
                if (file_exists($controllerFile)) {
                    include_once ($controllerFile);
                }
                
                // створюємо об'єкт классу контролера
                $controllerObject = new $controllerName;
                
                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);
                
                if ($result != NULL) {
                    break;
                }
            }
        }
        
    }
    
}
